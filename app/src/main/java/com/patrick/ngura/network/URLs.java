package com.patrick.ngura.network;

/**
 * Created by Juma on 10/23/2016.
 */

public class URLs {
    //BASE URL
  //  public static final String BASE_URL = "http://192.168.1.110:8888/api/v1/";

    public static final String BASE_URL = "http://192.168.42.190/ngura/public/api/v1/";


    //User login URL
    public static final String LOGIN_URL = BASE_URL + "login";

    //Register new User URL
    public static final String REGISTER_URL = BASE_URL + "register";

    //Update User Profile URL
    public static final String UPDATE_PROFILE_URL = BASE_URL + "updateprofile";

    //Update User Profile URL
    public static final String FETCH_ADS = BASE_URL + "test";
    //earn per hit
    public static final String EARNING_URL = BASE_URL + "earns";
    //earnings
    public static final String TOTAL_EARNING_URL = BASE_URL + "earnings";

}
