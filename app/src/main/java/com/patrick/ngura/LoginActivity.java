package com.patrick.ngura;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.patrick.ngura.network.AppController;
import com.patrick.ngura.network.CustomRequest;
import com.patrick.ngura.network.URLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class LoginActivity extends Activity {
    Button login;
    ProgressDialog pDialog;
    EditText loginusername, loginpassword;
    String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (Button) findViewById(R.id.btn_login);
        loginusername = (EditText) findViewById(R.id.username);
        loginpassword = (EditText) findViewById(R.id.password);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
                pDialog = new ProgressDialog(LoginActivity.this);
                pDialog.setMessage("Logging In...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
                // Toast.makeText(getApplicationContext(),"new",Toast.LENGTH_SHORT).show();
                //timer for progress dialog
                long delayInMillis = 5000;
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        pDialog.dismiss();


                    }
                }, delayInMillis);
                Toast.makeText(LoginActivity.this, "WELCOME"+username, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);

            }
        });
    }

    public void attemptLogin() {
        username = loginusername.getText().toString();
        password = loginpassword.getText().toString();
        //Check for empty Edit Text fields
        //Check Phone Number
        if (TextUtils.isEmpty(username)) {
            loginusername.setError("Enter username");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            loginpassword.setError("Enter password");
            return;
        }

        //login();
    }

    public void login() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Signing In...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URLs.LOGIN_URL, params, new Response.Listener<JSONObject>() {
            int success;

            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    success = response.getInt("id");
                    if (success >= 1) {
                        Log.d("Login Successful!", response.toString());
                        final int id = response.getInt("id");
                        final String firstname = response.getString("name");
                        Toast.makeText(LoginActivity.this, firstname, Toast.LENGTH_SHORT).show();
//                                final String lastname = response.getString("last_name");

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        finish();
                        startActivity(intent);


                    } else {
                        pDialog.dismiss();
                        Log.d("Login Failure!", response.toString());
                        Toast.makeText(LoginActivity.this, "Incorrect username OR password", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError response) {
                pDialog.dismiss();
                Log.d("Response: ", response.toString());
                Toast.makeText(LoginActivity.this, "NETWORK ERROR", Toast.LENGTH_SHORT).show();

                Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(login);

            }
        });
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}