package com.patrick.ngura.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.patrick.ngura.R;

import java.util.Timer;
import java.util.TimerTask;


public class NewProduct extends Fragment {
    ProgressDialog pDialog;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_product, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button save= (Button) view.findViewById(R.id.add_new_product_btn);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("loading...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
                // Toast.makeText(getApplicationContext(),"new",Toast.LENGTH_SHORT).show();
                //timer for progress dialog
                long delayInMillis = 5000;
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        pDialog.dismiss();
                        Toast.makeText(getActivity(), "PRODUCT ADDED", Toast.LENGTH_SHORT).show();
                        FragmentManager fm = getFragmentManager();
                        fm.beginTransaction().replace(R.id.flContent, new HomeFragment()).commit();
                    }
                }, delayInMillis);
            }
        });
    }

}